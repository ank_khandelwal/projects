﻿
#include "Windows.h"
#include "Winuser.h"
#include <stdlib.h>
#include <glut.h>
#include <glm/glm.hpp>
#include <glm/gtx/component_wise.hpp>  
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "Leap.h"

using namespace std;
using namespace glm;
using namespace Leap;


//Draw grid lines

const float sizeL = 100.f;
const float plane = 1.f;
void surface_plane() {
	glPointSize(sizeL);
	glColor3f(234, 240, 210);
	glBegin(GL_LINES);
	for (float i = -1000; i < 1000; i += plane)
	{
		glVertex3f(-1000.f, i, 0.f);
		glVertex3f(100.f, i, 0.f);

		glVertex3f(i, -1000.f, 0.f);
		glVertex3f(i, 100.f, 0.f);
	}
	glEnd();
}
struct Vertex
{
	vec3 position;
	vec3 normal;
};

vector< Vertex > LoadM(istream& in)
{
	vector< Vertex > verts;

	map< int, vec3 > positions;

	string lineStr;
	while (getline(in, lineStr))
	{
		istringstream lineSS(lineStr);
		string lineType;
		lineSS >> lineType;

		// parse vertex line
		if (lineType == "Vertex")
		{
			int idx = 0;
			float x = 0, y = 0, z = 0;
			lineSS >> idx >> x >> y >> z;
			positions.insert(make_pair(idx, vec3(x, y, z)));
		}

		// parse face line
		if (lineType == "Face")
		{
			int indexes[3] = { 0 };
			int idx = 0;
			lineSS >> idx >> indexes[0] >> indexes[1] >> indexes[2];


			vec3 U(positions[indexes[1]] - positions[indexes[0]]);
			vec3 V(positions[indexes[2]] - positions[indexes[0]]);
			vec3 faceNormal = normalize(cross(U, V));

			for (size_t j = 0; j < 3; ++j)
			{
				Vertex vert;
				vert.position = vec3(positions[indexes[j]]);
				vert.normal = faceNormal;
				verts.push_back(vert);
			}
		}
	}

	return verts;
}


int number_hands = 0;

class SampleListener : public Listener {
public:
	virtual void onConnect(const Controller&);
	virtual void onFrame(const Controller&);
};
float diff_z = 0;
ivec2 start_rotation, current_rotation;
ivec2 start_camera, current_camera;

void coordinate_axis(){
	//z axis

	glPushMatrix();
	glColor3f(0, 0, 255);
	GLUquadricObj *zobj = gluNewQuadric();
	gluCylinder(zobj, 0.1, 0.1, 6, 3, 4);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0, 0, 1.5);
	glPopMatrix();

	//x axis
	glPushMatrix();
	glColor3f(255, 0, 0);
	glRotated(90, 0, 1, 0);
	GLUquadricObj *xobj = gluNewQuadric();
	gluCylinder(xobj, 0.1, 0.1, 6, 3, 4);
	glTranslatef(0, 0, 1.5);
	glPopMatrix(); 

	//y axis

	glPushMatrix();
	glColor3f(0, 255, 0);
	glRotated(-90, 1, 0, 0);
	GLUquadricObj *yobj = gluNewQuadric();
	gluCylinder(yobj, 0.1, 0.1, 6, 3, 4);
	glTranslatef(0, 0, 1.5);
	surface_plane();
	glPopMatrix();
}


int Leap_gesture;
int Frame_pointables;

Vector initial_position;
bool flag = false;

static GLfloat  zooming_factor = 1.0f;

void  SampleListener::onFrame(const Controller& controller) {

	const Frame frame = controller.frame();
	HandList hands = frame.hands();					//to detect hand in the frame
	Hand hand = frame.hands().frontmost();
	Hand hand_left = frame.hands().leftmost();
	Hand hand_right = frame.hands().rightmost();
	Vector hand_left_pos = hand_left.palmPosition();
	Vector hand_right_pos = hand_right.palmPosition();

	Vector position = hand.palmPosition();
	number_hands = frame.hands().count();

	int appWidth = 800;
	int appHeight = 600;

	InteractionBox iBox = controller.frame().interactionBox();
	Pointable pointable = controller.frame().pointables().frontmost();
	Vector leapPoint = hand.stabilizedPalmPosition();

	Vector normalizedPoint = iBox.normalizePoint(leapPoint, false);

	int appX = normalizedPoint.x * appWidth;
	int appY = (1 - normalizedPoint.y) * appHeight;

	if (hand.isValid())
	{
		if (hand.grabStrength() > 0.4)
		{
			if (flag == false)
			{
				initial_position = hand.palmPosition();
				flag = true;
			}
		}
	}
	
	if (flag)
	{
		
		if (hands.count() == 1 && hand.isRight())
		{
			if (hand.grabStrength() > 0.4)
			{
				current_camera[0] += (position[0] - initial_position[0]);

				current_camera[1] += (position[1] - initial_position[1]);
			}
		}

		if (hands.count() == 1 && hand.isLeft())
		{


			if (hand.grabStrength() > 0.4)
			{
				current_rotation[0] += (position[0] - initial_position[0]);
				current_rotation[1] += (position[1] - initial_position[1]);

			}
		}

		if (hands.count() == 2)
		{
			if (hand_left.isValid() && hand_right.isValid())
				{
					if (hand_left.grabStrength() < 0.4 && hand_right.grabStrength() < 0.4)
					{
						diff_z = hand_right_pos[2] - hand_left_pos[2];
						
						zooming_factor = ((diff_z / 200) * 9) + 1;
						zooming_factor = 1 - ((abs(diff_z) / 200) * 0.9);
						
					}
				}
		}
	}
				
			initial_position = position;

		
			glutPostRedisplay();
}


vector< Vertex > model;

void Mesh_viewer()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	double w = glutGet(GLUT_WINDOW_WIDTH);

	double h = glutGet(GLUT_WINDOW_HEIGHT);
	double ar = w / h;

	glTranslatef(current_camera.x / w * 2, current_camera.y / h * 2, 0);
	gluPerspective(60, ar, 0.1, 20);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0, 0, -13);

	glPushMatrix();



	glRotatef(current_rotation.x % 360, 0, 1, 0);
	glRotatef(-current_rotation.y % 360, 1, 0, 0);

	glScalef(zooming_factor, zooming_factor, zooming_factor);
	glColor3ub(234, 240, 210);

	// draw model
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &model[0].position);
	glNormalPointer(GL_FLOAT, sizeof(Vertex), &model[0].normal);
	glDrawArrays(GL_TRIANGLES, 0, model.size());
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	// draw bounding cube
	glDisable(GL_LIGHTING);
	glColor3ub(255, 255, 255);
	//glutWireCube(7);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-5, -5, -5);
	coordinate_axis();
	glEnable(GL_LIGHTING);

	glPopMatrix();

	glutFullScreen();
	glutSwapBuffers();
}

// return the x/y/z min/max of some geometry
template< typename Vec >
pair< Vec, Vec > GetExtents
(
const Vec* pts,
size_t stride,
size_t count
)
{
	typedef typename Vec::value_type Scalar;
	Vec pmin(std::numeric_limits< Scalar >::max());
	Vec pmax(std::min(std::numeric_limits< Scalar >::min(),
		(Scalar)-std::numeric_limits< Scalar >::max()));

	// find extents
	unsigned char* base = (unsigned char*)pts;
	for (size_t i = 0; i < count; ++i)
	{
		const Vec& pt = *(Vec*)base;
		pmin = glm::min(pmin, pt);
		pmax = glm::max(pmax, pt);
		base += stride;
	}

	return make_pair(pmin, pmax);
}

// centers geometry around the origin
// and scales it to fit in a size^3 box
template< typename Vec >
void CenterAndScale
(
Vec* pts,
size_t stride,
size_t count,
const typename Vec::value_type& size
)
{
	typedef typename Vec::value_type Scalar;

	// get min/max extents
	pair< Vec, Vec > exts = GetExtents(pts, stride, count);

	// center and scale 
	const Vec center = (exts.first * Scalar(0.5)) + (exts.second * Scalar(0.5f));

	const Scalar factor = size / glm::compMax(exts.second - exts.first);
	unsigned char* base = (unsigned char*)pts;
	for (size_t i = 0; i < count; ++i)
	{
		Vec& pt = *(Vec*)base;
		pt = ((pt - center) * factor);
		base += stride;
	}
}




void SampleListener::onConnect(const Controller& controller) {
	std::cout << "Connected" << std::endl;
	controller.enableGesture(Gesture::TYPE_SWIPE);
	//test 1

	controller.config().setFloat("Gesture.Swipe.MinLength", 10.0);	//Min length in 'mm'
	controller.config().setFloat("Gesture.Swipe.MinVelocity", 100);	//MIn velocity in 'mm/sec'
	controller.config().save();

}

int main(int argc, char **argv)
{
	ifstream ifile("gargoyle.m");
	model = LoadM(ifile);
	if (model.empty())
	{
		cerr << "Error: No model selected" << endl;
		return -1;
	}
		Controller controller;
	SampleListener listener;
	controller.addListener(listener);


	CenterAndScale(&model[0].position, sizeof(Vertex), model.size(), 7);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(640, 480);
	glutCreateWindow("GLUT");
	glutSetCursor(GLUT_CURSOR_NONE);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	GLfloat position[] = { 0, 0, 1, 0 };
	glLightfv(GL_LIGHT0, GL_POSITION, position);

	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_LINE);
	glutDisplayFunc(Mesh_viewer);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // For the wire rendering
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glutMainLoop();
	controller.removeListener(listener);
	return 0;
}

