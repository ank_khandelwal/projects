/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "audio.h"
#include "oled.h"
#include "sleep.h"
#include <stdlib.h>
#include <math.h>
#include "xtime_l.h"
#include "xil_io.h"
#include "xparameters.h"

#define timer_base 0xF8F00000

//Timer Registers
static volatile int *timer_counter_l=(volatile int *)(timer_base+0x200);
static volatile int *timer_counter_h=(volatile int *)(timer_base+0x204);
static volatile int *timer_ctrl=(volatile int *)(timer_base+0x208);

//Function definitions
void init_timer(volatile int *timer_ctrl, volatile int *timer_counter_l, volatile int *timer_counter_h){
        *timer_ctrl=0x0;
        *timer_counter_l=0x1;
        *timer_counter_h=0x0;
        DATA_SYNC;
}

void start_timer(volatile int *timer_ctrl){
        *timer_ctrl=*timer_ctrl | 0x00000001;
        DATA_SYNC;
}

void stop_timer(volatile int *timer_ctrl){
        *timer_ctrl=*timer_ctrl & 0xFFFFFFFE;
        DATA_SYNC;
}
//void print(char *str);
#define PI	M_PI	/* pi to machine precision, defined in math.h */
#define TWOPI	(2.0*PI)

double X[256]; // Array to store imaginary and complex value
double O[128]; // Array to store output value after all the manupulations
double W[8][128]; // 2-D array for window averaging

void calculate_fft(double data[], int nn) {
	register unsigned int n, mmax, m, j, istep, i;
	// Double precision for the the trigonometric recurrences
	double wtemp, wr, wpr, wpi, wi, theta;
	double tempr, tempi;

	n = nn << 1;
	j = 1;

	// Bit reversal for the input data so that the real values are in even index.
	// And imaginary values are in odd index. The first value is ignored.
	for (i = 1; i < n; i += 2) {
		if (j > i) {
			tempr = data[j];
			data[j] = data[i];
			data[i] = tempr;
			tempr = data[j + 1];
			data[j + 1] = data[i + 1];
			data[i + 1] = tempr;
		}
		m = n >> 1;
		while (m >= 2 && j > m) {
			j -= m;
			m >>= 1;
		}
		j += m;
	}
	//Danielson-Lanzcos routine
	mmax = 2;
	//Outer loop is executed log2(nn) times
	while (n > mmax) {
		istep = 2 * mmax;
		theta = TWOPI / (mmax);
		// Initialize the trigonometric recurrence
		wtemp = sin(0.5 * theta);
		wpr = -2.0 * wtemp * wtemp;
		wpi = sin(theta);
		wr = 1.0;
		wi = 0.0;
		//internal loop
		for (m = 1; m < mmax; m += 2) {
			for (i = m; i <= n; i += istep) {
				j = i + mmax;
				//Danielson-Lanzcos formula
				tempr = wr * data[j] - wi * data[j + 1];
				tempi = wr * data[j + 1] + wi * data[j];
				data[j] = data[i] - tempr;
				data[j + 1] = data[i + 1] - tempi;
				data[i] += tempr;
				data[i + 1] += tempi;
			}
			// Trigonometric recurrence
			wr = (wtemp = wr) * wpr - wi * wpi + wr;
			wi = wi * wpr + wtemp * wpi + wi;
		}
		mmax = istep;
	}
}

void fft(short * audio_data) {

	register unsigned int Nx = 128,i;

	for (i = 0; i < Nx; i++) {
		// Copy the audio input to the even index of X[]
		X[2 * i + 1] = audio_data[i];
		// Initializing the imaginary value to zero.
		X[2 * i + 2] = 0.0;
	}


	// function to calculate fft
	calculate_fft(X, Nx);

}

void magnitude(void);
void noise_cancel(void);
void window_averaging(void);
unsigned int count;
int main(int argc, char * argv[]) {

	Xint16 audio_data[128];
	register unsigned int i;
	u8 *oled_equalizer_buf = (u8 *) malloc(128 * sizeof(u8));
	Xil_Out32(OLED_BASE_ADDR, 0xff);
	OLED_Init(); //oled init
	IicConfig(XPAR_XIICPS_0_DEVICE_ID);
	AudioPllConfig(); //enable core clock for ADAU1761
	AudioConfigure();
	//----------------------------------------------------------------------


	xil_printf("ADAU1761 configured\n\r");

	/*
	 * perform continuous read and writes from the codec that result in a loopback
	 * from Line in to Line out
	 */

	while (1) {
		 //Initialize the timer for performance monitoring
		    init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
		    start_timer(timer_ctrl);

		get_audio(audio_data);
		fft(audio_data);
		magnitude();
		noise_cancel();
		window_averaging();

		for (i = 0; i < 128; i++)
		{
			oled_equalizer_buf[i] = (Xint16) O[i]; // Output of the window-averaging to the OLED.
		}
		 stop_timer(timer_ctrl);
			    //Calculate the time for the operation
			    xil_printf("Communication time %d us\n\r", (*timer_counter_l)/333);
			//----------------------------------------------------------

		OLED_Clear();
		OLED_Equalizer_128(oled_equalizer_buf);

	}


	return 0;
}

// Magnitude module

void magnitude(void) {
	int i;
	for (i = 0; i < 128; i++) {
		// Magnitude = real*real + imaginary*imaginary
		O[i] = X[2 * i + 1] * X[2 * i + 1] + X[2 * i + 2] * X[2 * i + 2];

	}

}
//Noise cancellation module

void noise_cancel(void) {
	register unsigned int upper_threshold = 40;
	int lower_threshold = 600;
	register unsigned int i;
	for (i = 0; i < 128; i++) {
		O[i] = O[i] - lower_threshold; // To cancel the noise.
		if (O[i] < 0) {
			O[i] = 0; // If value less than 0 then setting it to zero.
		}
		//If greater than upper_threshold set it equal to upper threshold.
		if (O[i] > upper_threshold) {
			O[i] = upper_threshold;
		}

	}
}
// Averaging module

void window_averaging(void) {


	register unsigned int i, k;
   	for (i = 0; i < 128; i++) {
   		// Copying first 128 output samples to the Window 0 and so on till Window 7.
		W[count][i] = O[i];
	}


		for (i = 0; i < 128; i++) {
			for (k = 0; k< 8; k++) {
				O[i] += W[k][i];

			}
			O[i] /= 8; // Averaging over 8 window
		}
		count = (count++)%8; // Count = 0  after all the window elements are filled.
}
